### Command yang sering lupa

1. Paste text/output ke [Pastebin](https://pastebin.com) melalui terminal dengan curl.

  `command | curl -F c=@- https://ptpb.pw`

  Contoh : `echo "Mantap!" | curl -F c=@- https://ptpb.pw`

2. Resize tmpfs temporarily

   `mount -o remount,size=4G,noatime /tmp`

   Lihat mounted filesystem: `df -h`

3. Dll.
